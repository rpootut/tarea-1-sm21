﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modulo3.Clases
{
    abstract class Persona : Entidad
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
    }
}
