﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modulo3.Clases
{
    class Cliente : Persona
    {
        public string Direccion { get; set; }
        public string RFC { get; set; }
    }
}
