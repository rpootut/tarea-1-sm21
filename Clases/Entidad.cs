﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modulo3.Clases
{
    abstract class Entidad
    {
        public static List<Entidad> entidades = new List<Entidad>();
        public void Guardar()
        {
            entidades.Add(this);
        }
        public void Eliminar()
        {
            entidades.Remove(this);
        }
    }
}
