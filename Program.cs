﻿using Modulo3.Clases;
using System;

namespace Modulo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static void CrearCliente()
        {
            Cliente cliente = new Cliente();
            Console.WriteLine("Nuevo cliente");
            Console.Write("Nombre: ");
            cliente.Nombre = Console.ReadLine();
            Console.Write("Apellido: ");
            cliente.Apellido = Console.ReadLine();
            Console.Write("Direccion: ");
            cliente.Direccion = Console.ReadLine();
            Console.Write("RFC: ");
            cliente.RFC = Console.ReadLine();

            cliente.Guardar();

        }

        public static void ListarClientes()
        {
            foreach (Entidad entidad in Entidad.entidades)
            {
                Cliente cliente = (Cliente)entidad;
                Console.WriteLine($"El nombre es: {cliente.Nombre}");
                Console.WriteLine($"El RFC es: {cliente.RFC}");
            }
        }
    }
}
